from odoo import http


class BalanceController(http.Controller):
    @http.route('/balance', type='http', auth='public')
    def balance(self, **kwargs):
        # Perform the join operation manually
        query = """
            SELECT 
                act.id AS id,
                act.actual_value AS actual_actual_value, 
                act.date AS actual_date,
                kpi.bsc_id AS kpi_bsc_id,
                kpi.name AS kpi_name,
                kpi.perspective AS kpi_perspective,
                kpi.target AS kpi_target,
                kpi.threshold AS kpi_threshold,
                kpi.date AS kpi_date,
                ((act.actual_value / kpi.target) * 100) AS actual_score
            FROM 
                bsc_app_actual AS act 
            JOIN 
                bsc_app_kpi AS kpi ON act.kpi_id = kpi.id
        """

        # Execute the query and fetch the results
        env = http.request.env
        env.cr.execute(query)
        results = env.cr.dictfetchall()

        script_code = """
            <script>
                document.addEventListener("DOMContentLoaded", function() {
                    // Fetch the necessary data from the HTML table
                    var table = document.getElementById("tableId");
                    var labels = [];
                    var targetValues = [];
                    var actualValues = [];

                    for (var i = 1; i < table.rows.length; i++) {
                        labels.push(table.rows[i].cells[1].innerHTML);
                        targetValues.push(parseFloat(table.rows[i].cells[4].innerHTML));
                        actualValues.push(parseFloat(table.rows[i].cells[6].innerHTML));
                    }

                    // Create the chart
                    var ctx = document.createElement('canvas');
                    ctx.id = 'chart';
                    var chartContainer = document.getElementById('chartContainer');
                    chartContainer.appendChild(ctx);

                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [
                                {
                                    label: 'Target',
                                    data: targetValues,
                                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                                    borderColor: 'rgba(75, 192, 192, 1)',
                                    borderWidth: 1
                                },
                                {
                                    label: 'Actual',
                                    data: actualValues,
                                    backgroundColor: 'rgba(192, 75, 75, 0.2)',
                                    borderColor: 'rgba(192, 75, 75, 1)',
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });
                });
            </script>
        """

        response_data = f"""
        <!DOCTYPE html>
        <html>
        <head>
            <title>Balance Scorecard</title>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous" ></link>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous" ></script>
            <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        </head>
        <body>
        <div>
    <field name="plotly_chart" widget="plotly_chart" nolabel="1"/>
</div>
            <div style="min-width:100vw;" class="container m-5 border border-primary">
                <div class="mb-5" id="bscTable"></div>
                <div class="mb-5" id="kpiTable"></div>
                <div class="mb-5" id="meaTable"></div>
                <h2>Report</h2>
                <table class="table table-striped border" id="tableId">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>KPI Name</th>
                            <th>Perspective</th>
                            <th>Date</th>
                            <th>Target</th>
                            <th>Threshold</th>
                            <th>Actual Value</th>
                            <th>Actual Date</th>
                            <th>Score %</th>
                        </tr>
                    </thead>
                    <tbody>
        """

        for result in results:
            response_data += f"""
                        <tr>
                            <td>{result['id']}</td>
                            <td>{result['kpi_name']}</td>
                            <td>{result['kpi_perspective']}</td>
                            <td>{result['kpi_date']}</td>
                            <td>{result['kpi_target']}</td>
                            <td>{result['kpi_threshold']}</td>
                            <td>{result['actual_actual_value']}</td>
                            <td>{result['actual_date']}</td>
                            <td>{result['actual_score']:.2f}</td>
                        </tr>
            """

        response_data += """
                    </tbody>
                </table>
                <div class="container m-5" id="chartContainer">
                    <h2>Chart</h2>
                    <canvas id="chart"></canvas>
                </div>
            </div>
        </body>
        </html>
        """

        # Balance scorecard table
        bsce = http.request.env['bsc_app.bsc']
        bsc_records = bsce.search([])

        rows = ''
        for bsc in bsc_records:
            rows += f"""
                <tr>
                    <td>{bsc.organization.name}</td>
                    <td>{bsc.name}</td>
                    <td>{bsc.remarks}</td>
                </tr>
            """

        bsc_data = f"""
        <script>
            document.addEventListener("DOMContentLoaded", function() {{
                var table = document.getElementById("bscTable");
                table.innerHTML = `
                    <h2>Balance scorecard</h2>
                    <table class="table table-striped border mb-5" id="tableId">
                        <thead>
                            <tr>
                                <th>Organization</th>
                                <th>BSC Title</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            {rows}
                        </tbody>
                    </table>
                `;
            }});
        </script>
        """

        # KPI table
        Kpi = http.request.env['bsc_app.kpi']
        kpi_records = Kpi.search([])

        rows = ''
        for kpi in kpi_records:
            rows += f"""
                        <tr>
                            <td>{kpi.name}</td>
                            <td>{kpi.organization.name}</td>
                            <td>{kpi.bsc_id.name}</td>
                            <td>{kpi.perspective}</td>
                            <td>{kpi.objective.name}</td>
                            <td>{kpi.measure}</td>
                            <td>{kpi.initiative}</td>
                            <td>{kpi.target}</td>
                            <td>{kpi.threshold}</td>
                            <td>{kpi.date}</td>
                        </tr>
                    """

        kpi_data = f"""
                <script>
                    document.addEventListener("DOMContentLoaded", function() {{
                        var table = document.getElementById("kpiTable");
                        table.innerHTML = `
                            <h2>KPI</h2>
                            <table class="table table-striped border mb-5" id="tableId">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Organization</th>
                                        <th>BSC</th>
                                        <th>Perspective</th>
                                        <th>Objective</th>
                                        <th>Measure</th>
                                        <th>Initiative</th>
                                        <th>Target</th>
                                        <th>Threshold</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {rows}
                                </tbody>
                            </table>
                        `;
                    }});
                </script>
                """

        # Measurement table
        Actual = http.request.env['bsc_app.actual']
        actual_records = Actual.search([])

        # Generate HTML table rows from the fetched data
        rows = ''
        for actual in actual_records:
            rows += f"""
                        <tr>
                            <td>{actual.kpi_id.name}</td>
                            <td>{actual.date}</td>
                            <td>{actual.actual_value}</td>
                        </tr>
                    """

        # Construct the response HTML
        actual_data = f"""
                <script>
                    document.addEventListener("DOMContentLoaded", function() {{
                        var table = document.getElementById("meaTable");
                        table.innerHTML = `
                            <h2>Measurement</h2>
                            <table class="table table-striped border" id="tableId">
                                <thead>
                                    <tr>
                                        <th>KPI</th>
                                        <th>Date</th>
                                        <th>Actual Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {rows}
                                </tbody>
                            </table>
                        `;
                    }});
                </script>
                """

        response_data += script_code + bsc_data + kpi_data + actual_data

        return response_data
