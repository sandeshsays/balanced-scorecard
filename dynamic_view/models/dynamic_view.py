from odoo import fields, models, api, tools
# import plotly

class KpiMeasuremnentDynamicView(models.Model):
    _name = "kpi.measurement.dynamic.view"
    _description = "KPI Measurement dynamic view"
    _auto = False

    kpi_name = fields.Char("Name")
    kpi_perspective = fields.Selection([
        ('financial', 'Financial Perspective'),
        ('customer', 'Customer Perspective'),
        ('internal', 'Internal Process Perspective'),
        ('learning', 'Learning and Growth Perspective'),
    ])
    kpi_target = fields.Float("Target", (3, 2))
    kpi_threshold = fields.Float("Threshold", (3, 2))
    kpi_date = fields.Date(
        "Target Date",
        default=lambda self: fields.Date.today()
    )
    kpi_bsc_id = fields.Many2one("bsc_app.bsc",
                             string="BSC")

    actual_kpi_id = fields.Many2one("bsc_app.kpi", string="KPI", required=True,)
    actual_actual_value = fields.Float("Obtained Value", (3, 2))
    actual_date = fields.Date("Obtained Date",
                              default=lambda s: fields.Date.today(), )
    actual_score = fields.Float('Score %', (3, 2))

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
        create or replace view {} as (
            select act.id as id,
            act.actual_value as actual_actual_value, 
            act.date as actual_date,
            -- act.score as actual_score,
            kpi.bsc_id as kpi_bsc_id,
            kpi.name as kpi_name,
            kpi.perspective as kpi_perspective,
            kpi.target as kpi_target,
            kpi.threshold as kpi_threshold,
            kpi.date as kpi_date,
            ((act.actual_value/kpi.target)*100) as actual_score
            from bsc_app_actual as act 
            join bsc_app_kpi as kpi on act.kpi_id=kpi.id)
        """.format(self._table))

    # plotly_chart = fields.Text(
    #     string='Plotly Chart',
    #     compute='_compute_plotly_chart',
    # )

    # def _compute_plotly_chart(self):
    #     for rec in self:
    #         data = [{'x': [1, 2, 3], 'y': [2, 3, 4]}]
    #         rec.plotly_chart = plotly.offline.plot(data,
    #                                                include_plotlyjs=False,
    #                                                output_type='div')
