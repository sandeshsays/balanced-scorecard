{
    'name' : 'library_app',
    'author' : 'Sandesh',
    'depends' : ['base'],
    'category' : "Services/Library",
    'application' : True,
    'data' : [
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/book_view.xml',
        'views/book_list_template.xml',
        'reports/library_book_report.xml',
    ]
}