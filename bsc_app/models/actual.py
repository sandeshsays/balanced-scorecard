from odoo import fields, models, api
from odoo.exceptions import ValidationError


class Actual(models.Model):
    _name = 'bsc_app.actual'
    _description = 'Actual value'
    _inherit = ['mail.thread']

    kpi_id = fields.Many2one("bsc_app.kpi", string="KPI", required=True,)
    user_id = fields.Many2one("res.users", "User", default=lambda s: s.env.user,)
    date = fields.Date(default=lambda s: fields.Date.today(), )
    actual_value = fields.Float("Actual Value", (3, 2))
    score = fields.Float('Score %', (3, 2))

    @api.constrains('actual_value')
    def check_actual_value(self):
        for bsc in self:
            if not bsc.actual_value:
                raise ValidationError("Actual value is required!")
            if bsc.actual_value and not bsc.actual_value >= 1:
                raise ValidationError("%s actual_value is invalid\nNeeds to be greater than or equal to 1" % bsc.actual_value)
            return True

 #calculations part

    @api.depends('kpi_id.target', 'actual_value')
    def button_calculate(self):
        for measurement in self:
            if measurement.kpi_id and measurement.kpi_id.target != 0:
                measurement.score = (measurement.actual_value / measurement.kpi_id.target) * 100
            else:
                measurement.score = 0

    def get_score(self):
        return self.search([])

    def action_confirm(self):
        message = """Hello"""
        self.message_post(body=message)
