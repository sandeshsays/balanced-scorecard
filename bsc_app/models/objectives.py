from odoo import fields, models, api


class Objectives(models.Model):
    _name = 'bsc_app.objectives'
    _description = 'Objectives'

    bsc_id = fields.Many2one("bsc_app.bsc",
                                   string="BSC")
    perspective = fields.Selection([
        ('financial', 'Financial Perspective'),
        ('customer', 'Customer Perspective'),
        ('internal', 'Internal Process Perspective'),
        ('learning', 'Learning and Growth Perspective'),
    ])
    name = fields.Char("Title", required=True)
    objective = fields.Text(string="Objective", required=True)

