from odoo import fields, models, api
from odoo.exceptions import ValidationError


class Kpi(models.Model):
    _name = 'bsc_app.kpi'
    _description = 'Key Performance Indicator'
    _inherit = ['mail.thread']

    name = fields.Char("Title", required=True)
    organization = fields.Many2one("res.partner",
                                   string="Organization")
    bsc_id = fields.Many2one("bsc_app.bsc",
                             string="BSC")
    perspective = fields.Selection([
        ('financial', 'Financial Perspective'),
        ('customer', 'Customer Perspective'),
        ('internal', 'Internal Process Perspective'),
        ('learning', 'Learning and Growth Perspective'),
    ])
    objective = fields.Many2one("bsc_app.objectives", string="Objective")
    # objective = fields.Char()
    measure = fields.Char()
    initiative = fields.Char()
    target = fields.Float("Target", (3, 2))
    threshold = fields.Float("Threshold", (3, 2))
    date = fields.Date(
        "Date",
        default=lambda self: fields.Date.today()
    )

    @api.constrains('name')
    def check_name(self):
        for bsc in self:
            if not bsc.name:
                raise ValidationError("Name is not entered!")
            if bsc.name and not len(bsc.name) >= 3:
                raise ValidationError("%s name is invalid\nNeeds to be 3 chars or more" % bsc.name)
            return True

    @api.constrains('bsc_id')
    def check_bsc_id(self):
        for bsc in self:
            if not bsc.bsc_id:
                raise ValidationError("Scorecard is required!")
            return True

    @api.constrains('target')
    def check_target(self):
        for bsc in self:
            if not bsc.target:
                raise ValidationError("Target is required!")
            if bsc.target and not bsc.target >= 1:
                raise ValidationError("%s target is invalid\nNeeds to be greater than or equal to 1" % bsc.target)
            return True

    @api.constrains('threshold')
    def check_threshold(self):
        for bsc in self:
            if not bsc.threshold:
                raise ValidationError("Threshold is required!")
            if bsc.threshold and not bsc.threshold >= 1:
                raise ValidationError("%s threshold is invalid\nNeeds to be greater than or equal to 1" % bsc.threshold)
            return True

    def action_confirm(self):
        message = """Hello"""
        self.message_post(body=message)
