from odoo import models, fields, api
from odoo.exceptions import ValidationError

class Bsc(models.Model):
    _name = "bsc_app.bsc"
    _description = "A Balanced Scorecard"
    _order = "name, create_date desc"

    organization = fields.Many2one("res.partner",
                                      string="Organization")
    name = fields.Char("BSC Title", required=True)
    remarks = fields.Text(string="Remarks")


    @api.constrains('name')
    def check_name(self):
        for bsc in self:
            if not bsc.name:
                raise ValidationError("Name is required!")
            if bsc.name and not len(bsc.name) >= 3:
                raise ValidationError("%s name is invalid\nNeeds to be 3 chars or more" % bsc.name)
            return True

    @api.constrains('organization')
    def check_org(self):
        for bsc in self:
            if not bsc.organization:
                raise ValidationError("Organization is required!")
            return True
